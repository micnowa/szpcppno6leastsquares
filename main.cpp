#include "mainwindow.h"
#include <iostream>
#include <fstream>
#include <algorithm>
#include <cmath>
#include <boost/lambda/lambda.hpp>
#include <qwt/qwt_plot.h>
#include <qwt/qwt_plot_curve.h>
#include <qwt/qwt_point_data.h>
#include <QApplication>

using namespace std;

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    QWidget  okno;
    okno.setWindowTitle("Dopasowanie MNK");
    okno.setFixedSize(800, 600);

    vector <double> x,y;
    ifstream file_to_read;
    file_to_read.open("/home/ubuntu/Dokumenty/cpp/MNK#6/dane.dat");
    double tmp1, tmp2;
    int i = 0;
    while(file_to_read>>tmp1 && file_to_read>>tmp2)
    {
      x.push_back(tmp1);
      y.push_back(tmp2);
      i++;
    }
    double x_max = *(std::max_element(x.begin(), x.end()));
    double x_min = *(std::min_element(x.begin(), x.end()));
    double y_max = *(std::max_element(y.begin(), y.end()));
    double y_min = *(std::min_element(y.begin(), y.end()));

    QwtPlot wykres(&okno);
    wykres.setTitle("Dopasowanie MNK");
    wykres.setAxisTitle(QwtPlot::xBottom, "Nr kanału");
    wykres.setAxisTitle(QwtPlot::yLeft, "Napięcie[V]");
    wykres.setFixedSize(775, 600);
    wykres.setCanvasBackground(QBrush(QColor(0xff, 0xfa, 0x6b)));
    wykres.setAxisScale(QwtPlot::xBottom, x_min, x_max);
    wykres.setAxisScale(QwtPlot::yLeft, y_min, y_max);

    QwtPlotCurve dane_doswiadczalne;
    dane_doswiadczalne.setRawSamples(x.data(), y.data(), x.size());
    dane_doswiadczalne.setStyle(QwtPlotCurve::Dots);
    dane_doswiadczalne.setPen(QPen(Qt::blue, 3));
    dane_doswiadczalne.attach(&wykres);

    double A = 0;
    double B = 0;
    double x_sr = 0;
    double y_sr = 0;
    double xi_kw = 0;
    double xi_yi = 0;
    int N = x.size();
    //Czemu nie działa ?:(
    //std::for_each(x.begin(), x.end(), [&x_sr] (double &xi) {x_sr += xi;});
    //std::for_each(y.begin(), y.end(), [&y_sr] (double &yi) {y_sr += yi;});

    for(unsigned i=0; i < x.size(); i++)
    {
        xi_yi += y[i]*x[i];
        x_sr += x[i];
        y_sr += y[i];
        xi_kw += pow(x[i], 2);
    }
    x_sr /= N;
    y_sr /= N;

    A = ( xi_yi - N*x_sr*y_sr )/( xi_kw - N*pow(x_sr, 2) );
    B = y_sr - A*x_sr;
    cout<<A<<"  "<<B<<endl;

    vector<double> y_dop;
    for(unsigned i=0; i < x.size(); i++)
    {
        y_dop.push_back(A*x[i] + B);
    }
    QwtPlotCurve dopasowana_prosta;
    dopasowana_prosta.setRawSamples(x.data(), y_dop.data(), x.size());
    dopasowana_prosta.setStyle(QwtPlotCurve::Lines);
    dopasowana_prosta.setPen(QPen(Qt::red, 3));
    dopasowana_prosta.attach(&wykres);

    okno.show();

    return a.exec();
}

